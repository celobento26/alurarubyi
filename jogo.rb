def da_boas_vindas
    puts "Bem vindo ao jogo da adivinhação"
    puts "Qual é o seu nome?"
    nome = gets.strip
    puts "\n\n\n\n"
    puts "comecaremos o jogo para voce, #{nome}"
    nome.downcase
end

def sorteia_numero_secreto dificuldade

    if dificuldade == 1
        maximo = 30
    elsif dificuldade == 2
        maximo = 60
    elsif dificuldade == 3
        maximo = 100
    elsif dificuldade == 4
        maximo = 150
    else
        maximo = 200
    end

    puts "Escolhendo um número secreto entre 0 e #{maximo}..."

    numero_secreto = rand(maximo)
    puts "Escolhido... que tal adivinhar hoje nosso número secreto?"
    numero_secreto
end

def pede_um_numero chutes, tentativa, limite_de_tentativas
    puts "Chutes ate agora: #{chutes}"
    puts "Tentativa #{tentativa}", "Entre com o número"
    numero_escolhido = gets.strip
    puts "Será que acertou? Você chutou #{numero_escolhido}"
    numero_escolhido.to_i
end

def verifica_se_acertou numero_escolhido, numero_sortedo, nome_jogador
     acertou = numero_escolhido.to_i == numero_sortedo
     if nome_jogador == "marcelo"
        acertou = true
    end
    if acertou
        puts "Acertou!"
        return true
    else
        if numero_escolhido > numero_sortedo
            puts "Numero maior que o secreto"
        else
            puts "NUmero menor que o secreto"
        end
    end
    return false
end

def pede_dificuldade
    puts "Qual o nível de dificuldade que deseja? (1 fácil, 5 difícil)"
    dificuldade = gets.to_i
end

def nao_quer_jogar?
    puts "Deseja jogar novamente? (S/N)"
    quero_jogar = gets.strip
    quero_jogar.upcase == "N"
end

def joga nome_jogador, dificuldade

  numero_sortedo = sorteia_numero_secreto dificuldade

  limite_de_tentativas = 3

  chutes = []

  for tentativa in 1..limite_de_tentativas

      numero_escolhido = pede_um_numero chutes, tentativa, limite_de_tentativas
      chutes << numero_escolhido
       acertou = verifica_se_acertou(numero_escolhido.to_i, numero_sortedo, nome_jogador)


       if acertou == true

           break

       end

  end

  if acertou == false
      puts "Numero secreto foi #{numero_sortedo}"
  end
end

nome_jogador = da_boas_vindas
dificuldade = pede_dificuldade

loop do
  joga nome_jogador, dificuldade
  break if nao_quer_jogar?
end
